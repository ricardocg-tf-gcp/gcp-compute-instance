#instance = attribute('name')

control "gcp" do
    title "Google compute test"
    describe google_compute_instance(project: 'optimum-archery-284619', zone:'us-east1-b', name: 'vm-test') do
        its('machine_type') { should match "f1-micro" }
        its('status') { should eq 'RUNNING' }
        it { should exist }
    end
end