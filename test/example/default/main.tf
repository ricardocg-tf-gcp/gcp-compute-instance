
module "instance" {
    source          = "../../../"
    name            = "vm-test"
    machine_type    = "f1-micro"
    zone            = "us-east1-b"
    image           = "debian-cloud/debian-9"
}