# Compute Engine VM with web server 
# Compute Instance Module

- Creates a VM instance with web server. (Test aun falla con campos de attribute)

# Inputs 

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| Project ID | The ID of the Project| String | optimum.. |:yes:|
| Name | Name for the VM | String | vm-web-test |:yes:|
| Machine type | Type for the VM | f1-micro |  |:yes:|
| Zone | GCP zone to be deployed the resource | String | us-east1-b |:yes:|
| Image | Image for the VM| String | debian-cloud/debian-9 |:yes: |

# Outputs 

| Name | Description |
|------|-------------|
| IP | Map of the ID and CIDR for every subnet created |
| Name | Map of the ID and CIDR for every subnet created |
| ID | Map of the ID and CIDR for every subnet created |

# Usage

```js
module "instance" {
    source          = "../../../"
    name            = "vm-test"
    machine_type    = "f1-micro"
    zone            = "us-east1-b"
    image           = "debian-cloud/debian-9"
}
```