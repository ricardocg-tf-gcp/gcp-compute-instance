variable "project" {
    description     = "Project ID "
    default         = "optimum-archery-284619"
}

variable "name" {
    description     = "Name for the VM to create"
    default         = "vm-web-test"
}

variable "machine_type" {
    description     = "Type for the VM"
    default         = "f1-micro"
}

variable "zone" {
    description     = "Zone for the VM"
    default         = "us-east1-b"
}

variable "image" {
    description     = "Image for the VM"
    default         = "debian-cloud/debian-9"
}
