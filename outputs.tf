output "ip" {
  value = "${google_compute_instance.default.network_interface.0.access_config.0.nat_ip}"
}

output "name" {
  value = "${google_compute_instance.default.name}"
}

output "id" {
  value = "${google_compute_instance.default.instance_id}"
}